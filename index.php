<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
<title>Mahagun Mezzaria | Home </title>
<!--meta tags -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="Roof Houses Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web Template, 
         Smartphone Compatible web Template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        },
		false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
	
	
	
	
	
	
	
    </script>
		<!-- bootstrap style sheet -->
			<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- fontawesome -->
			<link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
		<!-- gallery css -->
			<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
		<!-- stylesheets-->
			<link href="css/style.css" rel='stylesheet' type='text/css' media="all">
		


		<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

	<style>
	.txt
	{
		font-siz: 12px;
		height: 30px;
		margin-bottom:6px;
	}
	.lconbox
	{
		position:fixed;
		top:20vh;
		right:40px;
		background:rgba(3,46,4,0.9);
		min-height:300px;
		width:300px;
		padding:15px;
		border-top:5px solid white;
		z-index:999;
	}
	.enq-box
	{
		position:fixed;
		bottom:0;
		left:0;
		width:100%;
		min-height:20px;
		z-index:999;
		display:none;
	}
	.enq-box .container
	{
		background:rgba(3,46,4,0.9);
		padding:15px 25px;
	}
	.enq-after-close
	{
		position:fixed;
		right:0;
		bottom:15vh;
		background:rgba(3,46,4,0.9);
		padding:5px 10px;
		height:auto;
		display:none;
		cursor:pointer;
		z-index:8;
	}
	.cd-top
	{
		z-index:10;
	}
</style>
	
	
</head>
<body>
<!-- nav -->
<header>
<nav class="navbar navbar-expand-lg navbar-light">
  <h1><a class="navbar-brand" href="index.php"><img src="image1/logo1.jpg" height="100%" width="100%"></a></h1>
	<a href="index.php"><h2><font color="yellow">Mahagun Mezzaria</font></h2></a>
	
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link scroll" href="index.php">HOME <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link scroll" href="#about">ABOUT</a>
      </li>
      <!--<li class="nav-item">
        <a class="nav-link scroll" href="#services">SERVICES</a>
      </li>
	  <!--<li class="nav-item">
        <a class="nav-link scroll" href="#team">TEAM</a>
      </li>-->
	  <li class="nav-item">
        <a class="nav-link scroll" href="#gallery">GALLERY</a>
      </li>
	  <!--<li class="nav-item">
        <a class="nav-link scroll" href="#stats">STATS</a>
      </li>-->
      <!--<li class="nav-item">
        <a class="nav-link scroll" href="#news">NEWS</a>
      </li>-->
	  <!--<li class="nav-item">
        <a class="nav-link scroll" href="#contact">CONTACT</a>
      </li>-->
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <h2><a href="index.php"> <img src="image1/project-logo.png"></a></h2>
    </ul>
  </div>
</nav>
</header>
<!-- //nav -->
<!-- banner -->
<div class="banner">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				  <ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				  </ol>
				  <div class="carousel-inner">
					<div class="carousel-item active">
					  <img class="d-block" src="image1/106-banner-Mahagun_Mezzaria.JPG" alt="First slide">
					<div class="carousel-caption ">
						<!--<h5>Modern Houses</h5>
						<p>Lorem Ipsum is simply dummy text</p>-->
					</div>
					</div>
					<div class="carousel-item">
					  <img class="d-block" src="image1/mezzarira-3.jpg" alt="Second slide">
					<div class="carousel-caption ">
							<!--<h5>Modern Villas</h5>
							<p>Lorem Ipsum is simply dummy text</p>-->
					</div>
					</div>
					<div class="carousel-item">
					  <img class="d-block" src="image1/mezzarira-8.jpg" alt="Third slide">
					<div class="carousel-caption">
							<!--<h5>Modern Apartments</h5>
							<p>Lorem Ipsum is simply dummy text</p>-->
					</div>		
					</div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>
</div>
<!-- //banner -->
<!-- about -->
			<div class="about py-lg-5 py-md-4 py-3" id="about">
				<div class="container py-sm-5 py-4">
				
				  <div class="row">
					<div class="col-md-6 col-sm about-left">
					  <img src="image1/mazz3.jpg" alt="image" />
					</div>
					<div class="col-md-6 col-sm about-right">
						<div class="title-heading-about">
							<h3>ABOUT US</h3>
							<h4>ABOUT</h4>
						</div>
					   <p>Mahagun Mezzaria is the latest project by Mahagun Group which brings all the facilities of high living at one place. Strategically located at Sector 78 Noida. Mahagun Mezzaria Noida is a wonderful opportunity for those looking for a space of their own at great prices. All the prominent places and the capital city of Delhi are within easy reach.   </p>
						<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary more-button" data-toggle="modal" data-target="#exampleModal33">
							  MORE INFO
							</button>

							<!-- Modal -->
							<div class="modal fade" id="exampleModal33" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">About</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">&times;</span>
									</button>
								  </div>
								  <div class="modal-body">
									<img src="image1/mazz3.jpg" alt="image" />
									<p>Mahagun Mezzaria is the latest project by Mahagun Group which brings all the facilities of high living at one place. Strategically located at Sector 78 Noida. Mahagun Mezzaria Noida is a wonderful opportunity for those looking for a space of their own at great prices. All the prominent places and the capital city of Delhi are within easy reach. Mahagun Mezzaria sector 78 Noida is equipped with the best facilities for modern living such as the latest security technologies, ACs, continuous power and water supply, parking space and much more. Mahagun Mezzaria is the ultimate designation for living in world class surrounding. It is the perfect blend of advanced architecture and technological amenities. Mahagun Mezzaria at Noida has the prime location for accessing commercial places and other important hubs. Mahagun Mezzaria provides a great ambience for a perfect living with greenery and beautiful surrounding. Mahagun Mezzaria Noida provides the ultimate in class living. Mahagun Builder is a leading developer engaged in real estate in the Delhi NCR region. The company is managed by highly skilled professionals with vast experience, providing high class living at affordable prices. The new project of Mahagun, Mahagun Mezzaria is perfectly made for high standard living at a strategically located place. Mahagun Mezzaria provides the best in secured and luxurious living at a great price.</p>
								  </div>
								  
								</div>
							  </div>
							</div>
					</div>
					
				  </div>
				</div>
			</div>
	
	<!--<div class="col-sm-12 col-md-12"><img src="image/Doc1.jpg" width="100%"></div>
					<div class="col-sm-12 col-md-12"><img src="image/Doc2.jpg" width="100%"></div>-->
<!-- about -->
<!-- services -->
<!--<section class="services py-lg-5 py-md-4 py-3" id="services">
	<div class="container py-sm-5 py-4">
				<div class="title-heading text-center mb-sm-5 mb-4">
					<h3>SERVICE</h3>
					<h4>SERVICE</h4>
				</div>
		<div class="row">
				<div class="col-lg-4 col-md-6 agileits_banner_bottom_left">
					<div class="agileinfo_banner_bottom_pos">
						<div class="row w3_agileits_banner_bottom_pos_grid">
							<div class="col-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out text-center">
									<i class="fab fa-steam" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits text-uppercase mb-2">Modern Houses</h4>
								<p class="mb-2">Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat egestas erat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg  text-uppercase" href="#" data-toggle="modal" data-target="#exampleModal33" role="button">Read More »</a>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 agileits_banner_bottom_left mt-md-0 mt-4">
					<div class="agileinfo_banner_bottom_pos">
						<div class="row w3_agileits_banner_bottom_pos_grid">
							<div class="col-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out text-center">
									<i class="fa fa-rocket" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits text-uppercase mb-2">Guest Houses</h4>
								<p class="mb-2">Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat egestas erat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg  text-uppercase" href="#" data-toggle="modal" data-target="#exampleModal33" role="button">Read More »</a>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 agileits_banner_bottom_left mt-lg-0 mt-md-5 mt-4">
					<div class="agileinfo_banner_bottom_pos">
						<div class="row w3_agileits_banner_bottom_pos_grid">
							<div class="col-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out text-center">
									<i class="fa fa-paint-brush" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits text-uppercase mb-2">Office Houses</h4>
								<p class="mb-2">Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat egestas erat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg  text-uppercase" href="#" data-toggle="modal" data-target="#exampleModal33" role="button">Read More »</a>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 agileits_banner_bottom_left mt-md-5 mt-4">
					<div class="agileinfo_banner_bottom_pos">
						<div class="row w3_agileits_banner_bottom_pos_grid">
							<div class="col-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out text-center">
									<i class="fas fa-industry" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits text-uppercase mb-2">Conceptual Design</h4>
								<p class="mb-2">Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat egestas erat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg  text-uppercase" href="#" data-toggle="modal" data-target="#exampleModal33" role="button">Read More »</a>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 agileits_banner_bottom_left mt-md-5 mt-4">
					<div class="agileinfo_banner_bottom_pos">
						<div class="row w3_agileits_banner_bottom_pos_grid">
							<div class="col-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out text-center">
									<i class="fa fa-fire" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits text-uppercase mb-2">Property Listing</h4>
								<p class="mb-2">Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat egestas erat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg  text-uppercase" href="#" data-toggle="modal" data-target="#exampleModal33" role="button">Read More »</a>
								
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 agileits_banner_bottom_left mt-md-5 mt-4">
					<div class="agileinfo_banner_bottom_pos">
						<div class="row w3_agileits_banner_bottom_pos_grid">
							<div class="col-3 wthree_banner_bottom_grid_left">
								<div class="agile_banner_bottom_grid_left_grid hvr-radial-out text-center">
									<i class="fas fa-cogs" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-9 wthree_banner_bottom_grid_right">
								<h4 class="sub_service_agileits text-uppercase mb-2">Quality Material</h4>
								<p class="mb-2">Morbi viverra lacus commodo felis semper, eu iaculis lectus feugiat egestas erat.</p>
								<div class="agileits-button two service">
									<a class="btn btn-primary btn-lg text-uppercase" href="#" data-toggle="modal" data-target="#exampleModal33" role="button">Read More »</a>
							
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</section>-->
<!-- //services -->
<!-- video section -->
    <!--<div class="video-w3l" data-vide-bg="video/1">
        <h2>We Make Your Dreams As Real</h2>
		<p>Contrary to popular belief, Lorem Ipsum is not simply random text</p>
			<!-- Button trigger modal -->
				<!--	<button type="button" class="btn btn-primary more-button3" data-toggle="modal" data-target="#exampleModal32">
					  MORE INFO
					</button>

					<!-- Modal -->
					<!--<div class="modal fade" id="exampleModal32" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							<img src="images/contact.jpg" alt="image" />
							<p class="model-body-text-p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
								standard dummy text.</p>
						  </div>
						  
						</div>
					  </div>
					</div>
    </div>-->
<!-- //video section -->
<!-- team -->
		<!--<div class="team py-lg-5 py-md-4 py-3" id="team">
			<div class="container py-sm-5 py-4">
			<div class="title-heading text-center mb-sm-5 mb-4">
				<h3>TEAM</h3>
				<h4>OUR TEAM</h4>
			</div>
				<div class="team_grids">
				<div class="row">
					<div class="col-md-3 team_grid text-center">
						<img src="images/t1.jpg" alt=" " class="img-fluid" />
						<h3>Amanda Seylon</h3>
						<p class="team_gridp">Designer</p>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<ul class="footer_list_icons team_icons">
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>

						</ul>
					</div>
					<div class="col-md-3 team_grid text-center">
						<img src="images/t2.jpg" alt=" " class="img-fluid" />
						<h3>Laura Mark</h3>
						<p class="team_gridp">Designer</p>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<ul class="footer_list_icons team_icons">
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>

						</ul>
					</div>
					<div class="col-md-3 team_grid text-center">
						<img src="images/t3.jpg" alt=" " class="img-fluid" />
						<h3>Lusiana James</h3>
						<p class="team_gridp">Designer</p>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<ul class="footer_list_icons team_icons">
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>

						</ul>
					</div>
					<div class="col-md-3 team_grid text-center">
						<img src="images/t4.jpg" alt=" " class="img-fluid" />
						<h3>John Mark</h3>
						<p class="team_gridp">Designer</p>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
						<ul class="footer_list_icons team_icons">
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>

						</ul>
					</div>
				 </div>
				</div>
			</div>
		</div>
		--><!-- team -->
 
 <!--gallery  -->
    <section class="wthree-row w3-gallery py-lg-5 py-md-4 py-3" id="gallery">
        <div class="container-fluid text-center">
            <div class="title-heading text-center">
				<h3>GALLERY</h3>
				<h4>GALLERY</h4>
				<br>
				<br>
			</div>
           <!-- <ul class="portfolio-categ filter pt-5 pt-sm-4 mb-5 mb-sm-4 text-center">
                <li class="port-filter all active">
                    <a href="#">All</a>
                </li>
                <li class="cat-item-1">
                    <a href="#" title="Category 1">Interior</a>
                </li>
                <li class="cat-item-2">
                    <a href="#" title="Category 2">Villas</a>
                </li>
                <li class="cat-item-3">
                    <a href="#" title="Category 3">Apartments</a>
                </li>
                <li class="cat-item-4">
                    <a href="#" title="Category 4">Properties</a>
                </li>
            </ul>-->
            <ul class="portfolio-area clearfix">
                <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">
                    <div>
                        <a class="image-gal" href="image1/mezzarira-1.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image1/mezzarira-1.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-1" data-type="cat-item-2">
                    <div>
                        <a class="image-gal" href="image1/mezzarira-4.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image1/mezzarira-4.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-2" data-type="cat-item-1">
                    <div>
                        <a class="image-gal" href="image1/mezzarira-10.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image1/mezzarira-10.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-3" data-type="cat-item-4">
                    <div>
                        <a class="image-gal" href="image1/mezzarira-12.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image1/mezzarira-12.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
				<!--<ul class="portfolio-area clearfix">
                <li class="portfolio-item2" data-id="id-1" data-type="cat-item-2">
                    <div>
                        <a class="image-gal" href="image/Manorial-3.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-3.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-2" data-type="cat-item-1">
                    <div>
                        <a class="image-gal" href="image/Manorial-4.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-4.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-3" data-type="cat-item-4">
                    <div>
                        <a class="image-gal" href="image/Manorial-5.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-5.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-4" data-type="cat-item-3">
                    <div>
                        <a class="image-gal" href="image/Manorial-6.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-6.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
				</ul>	
                <li class="portfolio-item2" data-id="id-5" data-type="cat-item-2">
                    <div>
                        <a class="image-gal" href="image/Manorial-7.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-7.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
			
                <li class="portfolio-item2" data-id="id-6" data-type="cat-item-1">
                    <div>
                        <a class="image-gal" href="image/Manorial-8.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-8.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-7" data-type="cat-item-1">
                    <div>
                        <a class="image-gal" href="image/Manorial-9.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-9.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-9" data-type="cat-item-4">
                    <div>
                        <a class="image-gal" href="image/Manorial-10.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-10.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
				 
                <li class="portfolio-item2" data-id="id-9" data-type="cat-item-3">
                    <div>
                        <a class="image-gal" href="image/Manorial-11.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-11.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-10" data-type="cat-item-2">
                    <div>
                        <a class="image-gal" href="image/lift lobby manorial.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/lift lobby manorial.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
                </li>
                <li class="portfolio-item2" data-id="id-8" data-type="cat-item-1">
                    <div>
                        <a class="image-gal" href="image/Manorial-1.jpg" data-gal="prettyPhoto[gallery]">

                            <img src="image/Manorial-1.jpg" height="100%" width="100%" class="img-fluid w3layouts agileits" alt="">

                        </a>
                    </div>
				</li>
            </ul>-->
            <!--end portfolio-area -->

        </div>
        <!-- //gallery container -->
    </section>
    <!-- //gallery -->
	<!-- Counter -->
<!--<div class="stats py-lg-5 py-md-4 py-3" id="stats">
<div class="container py-3">
			
	<div class="row">
	<div class=" col-lg-6 clients">
						<div class="title-heading-stats">
							<h3>OUR STATS</h3>
							<h4>STATS</h4>
						</div>
					   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                        standard dummy text.</p>
						<!-- Button trigger modal -->
							<!--<button type="button" class="btn btn-primary more-button1" data-toggle="modal" data-target="#exampleModal2">
							  MORE INFO
							</button>

							<!-- Modal -->
							<!--<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
							  <div class="modal-dialog" role="document">
								<div class="modal-content">
								  <div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel2">Modal title</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									  <span aria-hidden="true">&times;</span>
									</button>
								  </div>
								  <div class="modal-body">
									<img src="images/contact.jpg" alt="image" />
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
										standard dummy text.</p>
								  </div>
								  
								</div>
							  </div>
							</div>-->
	<!--</div>
	<div class="col-lg-6 services-bottom">
	<div class="row">
			<div class="col-md-6 agileits_w3layouts_about_counter_left text-center mb-5">
				<div class="countericon">
					<i class="fas fa-home"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">2601</p> 
					<h3>Properties</h3>
				</div>
				
			</div>
			<div class="col-md-6 agileits_w3layouts_about_counter_left text-center mb-5">
				<div class="countericon">
					<i class="far fa-smile"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">653</p> 
					<h3>Happy Customers</h3>
				</div>
				
			</div>
			
			<div class="col-md-6 agileits_w3layouts_about_counter_left text-center">
				<div class="countericon">
					<i class="fas fa-user"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">1024</p>
					<h3>Professional Agents</h3>
				</div>
				
			</div>
			<div class="col-md-6 agileits_w3layouts_about_counter_left text-center">
				<div class="countericon">
					<i class="fas fa-spinner"></i>
				</div>
				<div class="counterinfo">
					<p class="counter">2355</p>
					<h3>Success Stories</h3>
				</div>
			</div>
	</div>
	</div>
	</div>
</div>
</div>	-->
<!-- //Counter -->
<!-- Latest -->
<!--<div class="latest py-lg-5 py-md-4 py-3" id="news">
	<div class="container py-sm-5 py-4">
			<div class="title-heading text-center mb-sm-5 mb-4">
				<h3>LATEST NEWS</h3>
				<h4>NEWS</h4>
			</div>
		<div class="news_grids">
		<div class="row">
			<div class="col-md-4 mb-4 latest-news">
				<div class="news_grid1">
					<div class="news_grid_info">
						<p class="date">On Jan 20th, 2018</p>
						<h3>Office design</h3>
						<p>Vestibulum ut tincidunt lectus, vitae faucibus felis. Vivam uspretium leo eu iaculis lobortis.</p>
						<a href="#">Read more <span class="fa fa-long-arrow-right"></span></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-4 latest-news">
				<div class="news_grid2">
					<div class="news_grid_info">
						<p class="date">On Jan 20th, 2018</p>
						<h3>apartments design</h3>
						<p>Vestibulum ut tincidunt lectus, vitae faucibus felis. Vivam uspretium leo eu iaculis lobortis.</p>
						<a href="#">Read more <span class="fa fa-long-arrow-right"></span></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-4 latest-news">
				<div class="news_grid3">
					<div class="news_grid_info">
						<p class="date">On Jan 20th, 2018</p>
						<h3>Restaurant design</h3>
						<p>Vestibulum ut tincidunt lectus, vitae faucibus felis. Vivam uspretium leo eu iaculis lobortis.</p>
						<a href="#">Read more <span class="fa fa-long-arrow-right"></span></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 latest-news">
				<div class="news_grid4">
					<div class="news_grid_info">
						<p class="date">On Jan 20th, 2018</p>
						<h3>Office design</h3>
						<p>Vestibulum ut tincidunt lectus, vitae faucibus felis. Vivam uspretium leo eu iaculis lobortis.</p>
						<a href="#">Read more <span class="fa fa-long-arrow-right"></span></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 latest-news">
				<div class="news_grid5">
					<div class="news_grid_info">
						<p class="date">On Jan 20th, 2018</p>
						<h3>apartments design</h3>
						<p>Vestibulum ut tincidunt lectus, vitae faucibus felis. Vivam uspretium leo eu iaculis lobortis.</p>
						<a href="#">Read more <span class="fa fa-long-arrow-right"></span></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 latest-news">
				<div class="news_grid6">
					<div class="news_grid_info">
						<p class="date">On Jan 20th, 2018</p>
						<h3>Restaurant design</h3>
						<p>Vestibulum ut tincidunt lectus, vitae faucibus felis. Vivam uspretium leo eu iaculis lobortis.</p>
						<a href="#">Read more <span class="fa fa-long-arrow-right"></span></a>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>-->
<!-- //Latest -->
<!-- contact -->
	<!--	<div class="contact py-lg-5 py-md-4 py-3" id="contact">
		<div class="container py-sm-5 py-4">
			<div class="title-heading text-center mb-sm-5 mb-4">
				<h3>CONTACT</h3>
				<h4>CONTACT US</h4>
			</div>
			<div class="row">
				<div class="col-lg-4 col-sm-6">
				<div class="contact-left">
					<h3>About Us</h3>
					<p class="para-w3-agileits">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tortor est, malesuada at nulla in.</p>
					<h3>Find Us</h3>
					<div class="add-info">
						<p>1608 US-19, Ellaville, USA</p>
					</div>
					<div class="add-info">
						<p>+(012) 345 6789</p>
					</div>
					<div class="add-info">
						<p><a href="mailto:info@example.com">info@example.com</a></p>
					</div>
					<ul class="contact-social-icons">
						<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
				</div>
				</div>
				<div class="col-lg-8 col-sm-6">
				<div class="contact-middle">
					<h3>Write to Us</h3>
					<form action="#" method="post">
					  <div class="form-group">
						<label>Name</label>
						 <input type="text" class="form-control" placeholder="First name" required="">
					  </div>
					  <div class="form-group">
						<label>Email address</label>
						<input type="email" class="form-control"  placeholder="Email" required="">						
					  </div>
						<div class="form-group">
						<label>Example textarea</label>
							<textarea class="form-control" rows="3" required=""></textarea>
						</div>
					  <button type="submit" class="btn btn-2">Submit</button>
					</form>
				</div>
				
				</div>
		</div>
	</div>
	</div>
	-->
		<div class="lconbox">
                <form method="post" action="thank-you.php">
					<h6 style="text-align:center;color:white;">MAHAGUN MEZZARIA SECTOR 78, NOIDA<br>
						3/4 BHK APARTMENTS<br>
						<span style="color:aqua;">STARTING at Rs. 1.95Cr</span><br>
						CONTACT US</h6><hr>
                       <input type="text" placeholder="Name*" class="form-control txt" id="exampleFormControlInput1" name="name" required>
                       
                       <input type="text" placeholder="Email*" class="form-control txt" id="exampleFormControlInput1" name="email" required>
                       
                       <input type="text" placeholder="Mobile*" class="form-control txt" id="exampleFormControlInput1" name="mobile" required>
                       
                       <button class="btn btn-outline1 btn-xs" type="submit" name="sub">Send</button>
                       
                       <br><br>
                       <label class="exampleInputEmail1" style="color:white;font-size: 15px;text-align: center;">*Provided information is confidential and it will not be used for any promotional activity.</label>
	                   <br><center><a href="tel:+91-9871671495" style="text-decoration:none;color:aqua;font-weight:bold;">+91-9871671495</a></center>
                       </form>
             </div>
<!-- banner section -->
<div class='header'>
</div>
<!-- /banner section -->
<div class="enq-after-close">
	<span style="font-size:20px;color:white;">Enquiry</span>
</div>
<footer class="enq-box">
	<div class="container">
	<form method="post" action="thank-you.php">
	<a class="close" id="enqclose" style="font-size:60px;user-select:none;margin-top:-22px;">&times;</a>
    <h6 style="text-align:center;color:white;">MAHAGUN MEZZARIA SECTOR 78, NOIDA 3/4 BHK APARTMENTS <span style="color:aqua;">Starting at STARTING at Rs. 1.95Cr</span><br>
						CONTACT US</h6>
    	<div class="row">
    	<div class="col-sm-1"></div>
    	<div class="col-sm-3"><input type="text" placeholder="Name*" class="form-control txt" id="exampleFormControlInput1" name="name" required></div>
        <div class="col-sm-3"><input type="text" placeholder="Email*" class="form-control txt" id="exampleFormControlInput1" name="email" required></div>
        <div class="col-sm-3"><input type="text" placeholder="Mobile*" class="form-control txt" id="exampleFormControlInput1" name="mobile" required></div>
        <div class="col-sm-2"><button class="btn btn-outline1 btn-xs" type="submit" name="sub">Send</button></div>
		</div>	
        <center><label class="exampleInputEmail1" style="color:white;font-size:12px;">*Provided information is confidential and it will not be used for any promotional activity.</label>
	                   <br><a href="tel:+91-9871671495" style="text-decoration:none;color:white;font-weight:bold;">+91-9871671495</a></center>
	</form>    
    </div>
</footer>
		
		
	<!--amenties-->
	<div class="row">
		<div class="col-sm-1 col-md-1">
		</div>	
  		<div class="col-sm-5 col-md-5">
			<h3>KEY FEATURES</h3>
			<br>
			<p class="w-text">
			<ul>
			<font color="#000000"><b>
			<ul >
					<li>Architecture inspired by art deco design form & by architect Hafeez Contractor.</li>
					<li>IGBC certified project with GOLD standards.</li>
					<li>Aluminium shuttering technology in construction.</li>
					<li>Low density project upto 2 units per floor.</li>
					<li>Double height living rooms with 22 feet ceiling in the living area.</li>
					<li>Approx 80% open area.</li>
					<li>Full disclosure of built-up area.</li>
			<br>
			</ul>
			</b></font>
			</ul>
			<br>
			<br>
		</div>	
		<div class="col-sm-5 col-md-5">
			<h3>The Best Amenities</h3>
			<br>
			<p class="w-text">
			<ul>
			<font color="#000000"><b>
			<ul >
					<li>24 x 7 concierge service.</li>
					<li>VRF (Hot and Cold) air conditioning.</li>
					<li>Grand balconies overlooking greens.</li>
					<li>Luxuriously lavish master bedrooms.</li>
					<li>Exquisite clubhouse with Guest room facility, temperature controlled pool, sauna, steam, Jacuzzi, spa, business centre, banquet hall & restaurant.</li>
					<li>Indoor and outdoor recreational and sport options.</li>		    
			<br>
			</ul>
			</b>
			</font>
			</ul>
		</div>
		<div class="col-sm-1 col-md-1">
		</div>
		<br>
		<br>
		
		</div>
	<!--amenties-->
	
	
	
	
	<div class="map-w3ls">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14016.787258679826!2d77.382456!3d28.5638521!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x734f221fd2005fae!2sMahagun+Mezzaria+Apartment!5e0!3m2!1sen!2sin!4v1545798322238" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<!--<div class="w3layouts_newsletter_right">
		<div class="container">
		<div class="row">
			<div class="col-md-4 n-left">
				<h6>Newsletter</h6>
			</div>
			<div class="col-md-8 n-right">
				<form action="#" method="post">
					<div class="input-flds">
						<input type="text" name="Name" placeholder="Name" required="">
					</div>
					<div class="input-flds">
						<input type="email" name="Email" placeholder="Email" required="">
					</div>
					<div class="input-flds">
						<input type="submit" value="Subscribe">
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>-->
<!-- //contact -->
	</div>
	
	<!--footer -->
	
<div class="footer footer_w3layouts_section_1its">
	<div class="container">
		<div class="footer-top">
		<!--<div class="row">
			<div class="col-md-3 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>About Us</h3>
				</div>
				<div class="footer-text">
					<p>Curabitur non nulla sit amet nislinit tempus convallis quis ac lectus.Nulla porttitor accumsana tincidunt.</p>
					<ul class="social_section_1info">
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
							<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Contact</h3>
				</div>
				<div class="contact-info">
					<p><i class="fas fa-map-marker"></i>0926k 4th block building, king Avenue, New York City.</p>
					<p><i class="fas fa-phone"></i>Phone : +121 098 8907 9987</p>
					<p><i class="far fa-envelope"></i>Email : <a href="mailto:info@example.com">info@example.com</a></p>
				</div>
			</div>
			<div class="col-md-2 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Navigation</h3>
				</div>
				<ul class="links">
					<li><a href="index.html">Home</a></li>
					<li><a href="#about" class="scroll">About</a></li>
					<li><a href="#services" class="scroll">Services</a></li>
					<li><a href="#team" class="scroll">Team</a></li>
					<li><a href="#gallery" class="scroll">Gallery</a></li>
					<li><a href="#news" class="scroll">news</a></li>
					<li><a href="#contact" class="scroll">contact</a></li>
				</ul>
			</div>
			<div class="col-md-4 footer-grid_section_1its_w3">
				<div class="footer-title">
					<h3>Latest news</h3>
				</div>
				<div class="d-flex justify-content-around">
						<a href="#" class="col-4 fot_tp p-2">
							<img src="images/g1.jpg" class="img-fluid" alt="image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="images/g2.jpg" class="img-fluid" alt="image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="images/g3.jpg" class="img-fluid" alt="image">
						</a>
					</div>
					<div class="d-flex justify-content-around">
						<a href="#" class="col-4 fot_tp p-2">
							<img src="images/g4.jpg" class="img-fluid" alt="image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="images/g5.jpg" class="img-fluid" alt="image">
						</a>
						<a href="#" class="col-4 fot_tp p-2">
							<img src="images/g6.jpg" class="img-fluid" alt="image">
						</a>
					</div>

			</div>
			</div>
		</div>-->
		<div class="copyright">
			<p>© Mahagun Mezzaria 2018. All Rights Reserved | Design by <a href="http://w3layouts.com/">W3layouts</a> </p>
		</div>
	</div>
</div>
<!-- //footer -->


	<!-- js -->
		<script src="js/jquery.min.v3.js"></script>
		<script src="js/bootstrap.min.js"></script>
    <!-- //js -->
	
	
	<!-- stats -->
		<script src="js/jquery.waypoints.min.js"></script>
		<script src="js/jquery.countup.js"></script>
		<script>
			$('.counter').countUp();
		</script>
    <!-- //stats -->
	

	<!-- start-smooth-scrolling -->
    <script src="js/move-top.js"></script>
    <script src="js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        $(document).ready(function () {
            /*
            var defaults = {
            	containerID: 'toTop', // fading element id
            	containerHoverID: 'toTopHover', // fading element hover id
            	scrollSpeed: 1200,
            	easingType: 'linear' 
            };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src="js/SmoothScroll.min.js"></script>
	
    <!-- //smooth-scrolling-of-move-up -->
<!-- start-smooth-scrolling -->
    <script src="js/move-top.js"></script>
    <script src="js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        $(document).ready(function () {
            /*
            var defaults = {
            	containerID: 'toTop', // fading element id
            	containerHoverID: 'toTopHover', // fading element hover id
            	scrollSpeed: 1200,
            	easingType: 'linear' 
            };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src="js/SmoothScroll.min.js"></script>
	<script>
	var a=1;
	var wid=$(window).width();
	if(wid<728){
		$(".lconbox").fadeOut();
		$(".enq-box").fadeIn();
	}
	$(".enq-after-close").click(function(){
			$(".enq-box").fadeIn();
			$(".enq-after-close").fadeOut();
			a--;
		});
		$("#enqclose").click(function(){
			$(".enq-box").fadeOut();
			$(".enq-after-close").fadeIn();
			a++;
		});
			$(window).scroll(function(){
				var wid=$(window).width();
				if(wid>=728 && a==1)
				{
				var scr=$(window).scrollTop();
					if(scr>=20){
						$(".enq-box").fadeIn();
						$(".lconbox").fadeOut();
					}
					else if(scr<20){
						$(".lconbox").fadeIn();
						$(".enq-box").fadeOut();
					}
				}
				else if(wid<728 && a==1){
					$(".lconbox").fadeOut();	
					$(".enq-box").fadeIn();
				}
			});

	</script>
    <!-- //smooth-scrolling-of-move-up -->


	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
    <script src="js/jquery-1.7.2.js"></script>
    <script src="js/jquery.quicksand.js"></script>
    <script src="js/script.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->
	
	<!-- video js (background) -->
		<script src="js/jquery.vide.min.js"></script>
    <!-- //video js (background) -->
	
</body>
</html>